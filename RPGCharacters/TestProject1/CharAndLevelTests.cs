using RPGCharacters;
using RPGCharacters.Utility;
using System;
using Xunit;

namespace TestProject1
{
    public class CharAndLevelTests
    {
        [Fact]
        public void Char_Is_Level_1_When_Created()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            int expected = 1;
            //act
            int actual = testchar.Level;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Char_Is_Level_1_When_After_Gaining_A_Level()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            int expected = 2;
            //act
            testchar.LevelUp();
            int actual = testchar.Level;
            //Assert
            Assert.Equal(expected, actual);
        }
        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void Gaining_less_Than_One_Level_should_throw_ArgumentException_Theory(int value)
        {
            RPGCharacter testchar = new Warrior("Testchar");
            Assert.Throws<ArgumentException>(() => testchar.LevelUp(value));
        }
        [Fact]
        public void Mage_Starts_With_proper_Default_Attributes()
        {
            //Arrange
            RPGCharacter testchar = new Mage("Testchar");
            int[] expected = { 5, 1, 1, 8 };
            //act
            int[] actual = {testchar.TotalPrimaryAttribute.Vitality, testchar.TotalPrimaryAttribute.Strength,
                testchar.TotalPrimaryAttribute.Dexterity, testchar.TotalPrimaryAttribute.Intelligence};
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Ranger_Starts_With_proper_Default_Attributes()
        {
            //Arrange
            RPGCharacter testchar = new Ranger("Testchar");
            int[] expected = { 8, 1, 7, 1 };
            //act
            int[] actual = {testchar.TotalPrimaryAttribute.Vitality, testchar.TotalPrimaryAttribute.Strength,
                testchar.TotalPrimaryAttribute.Dexterity, testchar.TotalPrimaryAttribute.Intelligence};
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Rogue_Starts_With_proper_Default_Attributes()
        {
            //Arrange
            RPGCharacter testchar = new Rogue("Testchar");
            int[] expected = { 8, 2, 6, 1 };
            //act
            int[] actual = {testchar.TotalPrimaryAttribute.Vitality, testchar.TotalPrimaryAttribute.Strength,
                testchar.TotalPrimaryAttribute.Dexterity, testchar.TotalPrimaryAttribute.Intelligence};
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Warrior_Starts_With_proper_Default_Attributes()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            int[] expected = { 10, 5, 2, 1 };
            //act
            int[] actual = {testchar.TotalPrimaryAttribute.Vitality, testchar.TotalPrimaryAttribute.Strength,
                testchar.TotalPrimaryAttribute.Dexterity, testchar.TotalPrimaryAttribute.Intelligence};
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Mage_Has_Correct_Attributes_After_Leveling()
        {
            //Arrange
            RPGCharacter testchar = new Mage("Testchar");
            int[] expected = { 5 + 3, 1 + 1, 1 + 1, 8 + 5 };
            //act
            testchar.LevelUp();
            int[] actual = {testchar.TotalPrimaryAttribute.Vitality, testchar.TotalPrimaryAttribute.Strength,
                testchar.TotalPrimaryAttribute.Dexterity, testchar.TotalPrimaryAttribute.Intelligence};
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Ranger_Has_Correct_Attributes_After_Leveling()
        {
            //Arrange
            RPGCharacter testchar = new Ranger("Testchar");
            int[] expected = { 8 + 2, 1 + 1, 7 + 5, 1 + 1 };
            //act
            testchar.LevelUp();
            int[] actual = {testchar.TotalPrimaryAttribute.Vitality, testchar.TotalPrimaryAttribute.Strength,
                testchar.TotalPrimaryAttribute.Dexterity, testchar.TotalPrimaryAttribute.Intelligence};
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Rogue_Has_Correct_Attributes_After_Leveling()
        {
            //Arrange
            RPGCharacter testchar = new Rogue("Testchar");
            int[] expected = { 8 + 3, 2 + 1, 6 + 4, 1 + 1 };
            //act
            testchar.LevelUp();
            int[] actual = {testchar.TotalPrimaryAttribute.Vitality, testchar.TotalPrimaryAttribute.Strength,
                testchar.TotalPrimaryAttribute.Dexterity, testchar.TotalPrimaryAttribute.Intelligence};
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Warrior_Has_Correct_Attributes_After_Leveling()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            int[] expected = { 10 + 5, 5 + 3, 2 + 2, 1 + 1 };
            //act
            testchar.LevelUp();
            int[] actual = {testchar.TotalPrimaryAttribute.Vitality, testchar.TotalPrimaryAttribute.Strength,
                testchar.TotalPrimaryAttribute.Dexterity, testchar.TotalPrimaryAttribute.Intelligence};
            //Assert
            Assert.Equal(expected, actual);
        }
        [Fact]
        public void Warrior_Has_Correct_Secondary_Attributes_After_Leveling()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            int[] expected = { 150,12,2 };
            //act
            testchar.LevelUp();
            int[] actual = {testchar.SecondaryAttribute.Health,testchar.SecondaryAttribute.ArmorRating,testchar.SecondaryAttribute.ElementalResistance};
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
