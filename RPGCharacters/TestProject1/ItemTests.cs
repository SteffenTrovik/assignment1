using RPGCharacters;
using RPGCharacters.Utility;
using System;
using Xunit;

namespace TestProject1
{
    public class ItemTests
    {
        [Fact]
        public void Equipping_too_high_ilvl_weapon_returns_InvalidWEaponException()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            Weapon testAxe = new Weapon("Common Axe", 2, Item.Slots.Weapon, Item.ItemTypes.Axe, Weapon.WeaponTypes.Axe, 7, 1.1);
            //Assert
            Assert.Throws<InvalidWeaponException>(() => testchar.EqupItem(testAxe));
        }

        [Fact]
        public void Equipping_too_high_ilvl_armour_returns_InvalidArmourException()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            Armour testPlateBody = new Armour(new PrimaryAttributes(2, 1, 0, 0), "Common plate body armour", 2, Item.Slots.Body, Item.ItemTypes.Plate, Armour.ArmourTypes.Plate);
            //Assert
            Assert.Throws<InvalidArmourException>(() => testchar.EqupItem(testPlateBody));
        }

        [Fact]
        public void Equipping_Wrong_Weapontype_returns_InvalidWeaponException()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            Weapon testBow = new Weapon("Common Bow", 1, Item.Slots.Weapon, Item.ItemTypes.Bow, Weapon.WeaponTypes.Axe, 12, 0.8);
            //Assert
            Assert.Throws<InvalidWeaponException>(() => testchar.EqupItem(testBow));
        }

        [Fact]
        public void Equipping_Wrong_armourtype_InvalidArmourException()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            Armour testClothHead = new Armour(new PrimaryAttributes(1, 0, 0, 5), "Common cloth head armour", 1, Item.Slots.Head, Item.ItemTypes.Cloth, Armour.ArmourTypes.Cloth);
            //Assert
            Assert.Throws<InvalidArmourException>(() => testchar.EqupItem(testClothHead));
        }

        [Fact]
        public void Equipping_weapon_successfully_returns_New_weapon_equipped()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            Weapon testAxe = new Weapon("Common Axe", 1, Item.Slots.Weapon, Item.ItemTypes.Axe, Weapon.WeaponTypes.Axe, 7, 1.1);
            //act
            string expected = "Common Axe equipped!";
            string actual = testchar.EqupItem(testAxe);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Equipping_Armour_successfully_returns_new_Armour_equipped()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            Armour testPlateBody = new Armour(new PrimaryAttributes(2, 1, 0, 0), "Common plate body armour", 1, Item.Slots.Body, Item.ItemTypes.Plate, Armour.ArmourTypes.Plate);
            //act
            string expected = "Common plate body armour equipped!";
            string actual = testchar.EqupItem(testPlateBody);
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Dps_Without_Weapon_Should_Be_1()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            double expected = 1.0;
            //act
            double actual = testchar.DPS;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DPS_With_Weapon_Calculates_Correctly()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            double expected = (7.0*1.1)*(1.0+(5.0/100.0));
            Weapon testAxe = new Weapon("Common Axe", 1, Item.Slots.Weapon, Item.ItemTypes.Axe, Weapon.WeaponTypes.Axe, 7, 1.1);
            //act
            testchar.EqupItem(testAxe);
            double actual = testchar.DPS;
            //Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void DPS_With_Weapon_and_Armour_Calculates_Correctly()
        {
            //Arrange
            RPGCharacter testchar = new Warrior("Testchar");
            double expected = (7.0 * 1.1) * (1.0 + ((5.0+1.0) / 100.0));
            Armour testPlateBody = new Armour(new PrimaryAttributes(2, 1, 0, 0), "Common plate body armour", 1, Item.Slots.Body, Item.ItemTypes.Plate, Armour.ArmourTypes.Plate);
            Weapon testAxe = new Weapon("Common Axe", 1, Item.Slots.Weapon, Item.ItemTypes.Axe, Weapon.WeaponTypes.Axe, 7, 1.1);
            //act
            testchar.EqupItem(testAxe);
            testchar.EqupItem(testPlateBody);
            double actual = testchar.DPS;
            //Assert
            Assert.Equal(expected, actual);
        }
    }
}
