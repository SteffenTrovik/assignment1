﻿using RPGCharacters.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Creates a Rogue character and provides basestats to parent class
    /// </summary>
    public class Rogue : RPGCharacter
    {
        public Rogue(string name) : base(new PrimaryAttributes(LevelManager.RogueStartStats[0], LevelManager.RogueStartStats[1],
            LevelManager.RogueStartStats[2], LevelManager.RogueStartStats[3]), name)
        {
            this.UsableItems = EquipmentManager.RogueItems;
        }
    }
}
