﻿using RPGCharacters.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace RPGCharacters
{
    public abstract class RPGCharacter
    {
 //       public enum Classes { Mage, Ranger, Rogue, Warrior };
        public PrimaryAttributes BasePrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttribute { get; set; }
        public SecondaryAttributes SecondaryAttribute { get; set; }

        public double DPS { get; set; }
        public string Name { get; set; }
        public int Level { get; set; }

        public Item.ItemTypes[] UsableItems;
        public Dictionary<Item.Slots, Item> Equipment { get; set;
        }

/// <summary>
/// Base character constructor, initialized character and gives base attributes
/// </summary>
/// <param name="basePrimaryAttributes">tarting attributes provided by subclass</param>
/// <param name="name">name of character</param>
        public RPGCharacter(PrimaryAttributes basePrimaryAttributes, string name)
        {
            this.BasePrimaryAttributes = basePrimaryAttributes;
            this.TotalPrimaryAttribute = new PrimaryAttributes(0,0,0,0)+basePrimaryAttributes;
            this.SecondaryAttribute = new SecondaryAttributes().CalculateSecondaryAttributes(
                this.TotalPrimaryAttribute.Vitality, this.TotalPrimaryAttribute.Strength,
                this.TotalPrimaryAttribute.Dexterity, this.TotalPrimaryAttribute.Intelligence);
            this.Name = name;
            this.Level = 1;
            this.Equipment = new Dictionary<Item.Slots, Item>();
            DPSManager.CalculateDps(this);
        }
        /// <summary>
        /// Levels up character one time
        /// </summary>
        public void LevelUp()
        {
            LevelManager.Levelup(this);
        }

        /// <summary>
        /// Levels up character x amount of times
        /// </summary>
        /// <param name="amount">integer representing the amount of times the char is leveled up</param>
        public void LevelUp(int amount)
        {
            if(amount < 1)
            {
                throw new ArgumentException();
            }
            else
            {
                for (int i = 0; i < amount; i++)
                {
                    LevelManager.Levelup(this);
                }
            }
        }
        /// <summary>
        /// Generates all the character stats as a String
        /// </summary>
        /// <returns>Returns string containing stats</returns>
        public override string ToString()
        {
            return "Character Name:\t" + this.Name + "\nLevel:\t" + this.Level + "\nStrength:\t" + this.TotalPrimaryAttribute.Strength
                + "\nDexterity:\t" + this.TotalPrimaryAttribute.Dexterity + "\nIntelligence:\t" + this.TotalPrimaryAttribute.Intelligence
                + "\nHealth:\t\t" + this.SecondaryAttribute.Health + "\nArmor Rating:\t" + this.SecondaryAttribute.ArmorRating
                + "\nResistance:\t" + this.SecondaryAttribute.ElementalResistance + "\nDPS:\t"+Math.Round((this.DPS),1);
        }
    }
}
