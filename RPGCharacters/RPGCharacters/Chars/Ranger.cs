﻿using RPGCharacters.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Creates a Ranger character and provides basestats to parent class
    /// </summary>
    public class Ranger : RPGCharacter
    {
        public Ranger(string name) : base(new PrimaryAttributes(LevelManager.RangerStartStats[0], LevelManager.RangerStartStats[1],
            LevelManager.RangerStartStats[2], LevelManager.RangerStartStats[3]), name)
        {
            this.UsableItems = EquipmentManager.RangerItems;
        }
    }
}
