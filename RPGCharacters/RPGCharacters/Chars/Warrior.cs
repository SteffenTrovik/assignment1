﻿using RPGCharacters.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Creates a Warrior character and provides basestats to parent class
    /// </summary>
    public class Warrior : RPGCharacter
    {
        public Warrior(string name) : base(new PrimaryAttributes(LevelManager.WarriorStartStats[0], LevelManager.WarriorStartStats[1],
            LevelManager.WarriorStartStats[2], LevelManager.WarriorStartStats[3]), name)
        {
            this.UsableItems = EquipmentManager.WarriorItems;
        }
    }
}
