﻿using RPGCharacters.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    /// <summary>
    /// Creates a Mage character and provides basestats to parent class
    /// </summary>
    public class Mage : RPGCharacter
    {
        public Mage(string name) : base(new PrimaryAttributes(LevelManager.MageStartStats[0], LevelManager.MageStartStats[1],
            LevelManager.MageStartStats[2], LevelManager.MageStartStats[3]), name)
        {
            this.UsableItems= EquipmentManager.MageItems;
        }
    }
}
