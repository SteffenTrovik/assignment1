﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Utility
{
    public static class DPSManager
    {
        /// <summary>
        /// Calculates the dps of the character
        /// </summary>
        /// <param name="rpgChar">the character to update dps</param>
        /// <returns></returns>
        public static double CalculateDps(this RPGCharacter rpgChar)
        {
            double statScaler = 0.0;
            if (rpgChar.GetType() == typeof(Mage))
            {
                statScaler += rpgChar.TotalPrimaryAttribute.Intelligence;
            }
            else if (rpgChar.GetType() == typeof(Ranger))
            {
                statScaler += rpgChar.TotalPrimaryAttribute.Dexterity;
            }
            else if (rpgChar.GetType() == typeof(Rogue))
            {
                statScaler += rpgChar.TotalPrimaryAttribute.Dexterity;
            }
            else if (rpgChar.GetType() == typeof(Warrior))
            {
                statScaler += rpgChar.TotalPrimaryAttribute.Strength;
            }
            if (rpgChar.Equipment.TryGetValue(Item.Slots.Weapon, out Item wpn))
            {
                Weapon weapon = (Weapon)wpn;
                
                double dps = ((weapon.Damage * weapon.AttackSpeed) * (1.0 + statScaler / 100.0));
                rpgChar.DPS = dps;
                return dps;
            }
            else
            {
                rpgChar.DPS = 1.0;
                return 1.0;
            }
        }
    }
}
