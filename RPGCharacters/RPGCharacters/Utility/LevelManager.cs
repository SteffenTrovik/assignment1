﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Utility
{
    public static class LevelManager
    {
        public static readonly int[] MageStartStats = { 5, 1, 1, 8 };
        public static readonly int[] MageLevelStats = { 3, 1, 1, 5 };

        public static readonly int[] RangerStartStats = { 8, 1, 7, 1 };
        public static readonly int[] RangerLevelStats = { 2, 1, 5, 1 };

        public static readonly int[] RogueStartStats = { 8, 2, 6, 1 };
        public static readonly int[] RogueLevelStats = { 3, 1, 4, 1 };

        public static readonly int[] WarriorStartStats = { 10, 5, 2, 1 };
        public static readonly int[] WarriorLevelStats = { 5, 3, 2, 1 };

        /// <summary>
        /// Levels up a character, and gives different stats based on class
        /// </summary>
        /// <param name="rpgChar">the character to level up</param>
        public static void Levelup(this RPGCharacter rpgChar)
        {
            int[] startStats = { 0, 0, 0, 0 };
            int[] levelStats = { 0, 0, 0, 0, };

            if (rpgChar.GetType() == typeof(Mage))
            {
                startStats = MageStartStats;
                levelStats = MageLevelStats;
            }
            else if (rpgChar.GetType() == typeof(Ranger))
            {
                startStats = RangerStartStats;
                levelStats = RangerLevelStats;
            }
            else if (rpgChar.GetType() == typeof(Rogue))
            {
                startStats = RogueStartStats;
                levelStats = RogueLevelStats;
            }
            else if (rpgChar.GetType() == typeof(Warrior))
            {
                startStats = WarriorStartStats;
                levelStats = WarriorLevelStats;
            }

            rpgChar.Level++;
            rpgChar.BasePrimaryAttributes += new PrimaryAttributes(levelStats[0], levelStats[1], levelStats[2], levelStats[3]);
            rpgChar.TotalPrimaryAttribute += new PrimaryAttributes(levelStats[0], levelStats[1], levelStats[2], levelStats[3]);
            rpgChar.SecondaryAttribute.CalculateSecondaryAttributes(
                rpgChar.TotalPrimaryAttribute.Vitality, rpgChar.TotalPrimaryAttribute.Strength,
                rpgChar.TotalPrimaryAttribute.Dexterity, rpgChar.TotalPrimaryAttribute.Intelligence);
            rpgChar.SecondaryAttribute.updateSecondaryAttributes(rpgChar);
            DPSManager.CalculateDps(rpgChar);
        }
    }
}
