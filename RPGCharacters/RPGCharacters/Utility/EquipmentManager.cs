﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Utility
{
    public static class EquipmentManager
    {
        public static readonly Item.ItemTypes[] MageItems = { Item.ItemTypes.Staff, Item.ItemTypes.Wand, Item.ItemTypes.Cloth };
        public static readonly Item.ItemTypes[] RangerItems = { Item.ItemTypes.Bow, Item.ItemTypes.Leather, Item.ItemTypes.Mail };
        public static readonly Item.ItemTypes[] RogueItems = { Item.ItemTypes.Dagger, Item.ItemTypes.Sword, Item.ItemTypes.Leather, Item.ItemTypes.Mail };
        public static readonly Item.ItemTypes[] WarriorItems = { Item.ItemTypes.Axe, Item.ItemTypes.Hammer, Item.ItemTypes.Sword, Item.ItemTypes.Mail, Item.ItemTypes.Plate };

        /// <summary>
        /// Tries to equip an item on the character, can throw invalidweaponexception or invalidarmourexception
        /// </summary>
        /// <param name="rpgChar">the character to try to equip the item to</param>
        /// <param name="item">the item you want to try equip</param>
        /// <returns></returns>
        public static string EqupItem(this RPGCharacter rpgChar, Item item)
        {
            if ((item.ItemLevel > rpgChar.Level) || !rpgChar.UsableItems.Contains(item.ItemType))
            {
                if (item.GetType() == typeof(Weapon))
                {
                    throw new InvalidWeaponException();
                }
                else if (item.GetType() == typeof(Armour))
                {
                    throw new InvalidArmourException();
                }
                return "";
            }
            else
            {
                if (rpgChar.Equipment.ContainsKey(item.ItemSlot))
                {
                    rpgChar.Equipment[item.ItemSlot] = item;
                }
                else
                {
                    rpgChar.Equipment.Add(item.ItemSlot, item);
                }
                PrimaryAttributes freshStats = rpgChar.BasePrimaryAttributes;
                foreach (var i in rpgChar.Equipment.Values)
                {
                    if (i.GetType() == typeof(Armour)){
                        Armour j = (Armour)i;
                        freshStats += j.Attributes;
                    }
                }
                rpgChar.TotalPrimaryAttribute = freshStats;
                rpgChar.SecondaryAttribute.updateSecondaryAttributes(rpgChar);
                DPSManager.CalculateDps(rpgChar);
                //rpgChar.SecondaryAttribute.CalculateSecondaryAttributes();
                //return ("New " + item.GetType().Name + " equipped!");
                return (item.ItemName + " equipped!");
            }
        }
    }
}
