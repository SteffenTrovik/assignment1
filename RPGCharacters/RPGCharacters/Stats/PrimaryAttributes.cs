﻿namespace RPGCharacters
{
    public class PrimaryAttributes
    {
            public int Vitality { get; set; }
            public int Strength { get; set; }
            public int Dexterity { get; set; }
            public int Intelligence { get; set; }

        /// <summary>
        /// Creates an instance of attributes with given stats
        /// </summary>
        /// <param name="vitality">Gives life</param>
        /// <param name="strength">Gives dmg to warrior aswell as armour</param>
        /// <param name="dexterity">Gives dmg to rogue and ranger aswell as armour</param>
        /// <param name="intelligence">gives dmg to mage aswell as resistances</param>
            public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence)
            {
                this.Vitality = vitality;
                this.Strength = strength;
                this.Dexterity = dexterity;
                this.Intelligence = intelligence;
            }

        /// <summary>
        /// overloaded + operator to add all the stats together for easier leveling and adding of items
        /// </summary>
        /// <param name="lhs">Stats that exists</param>
        /// <param name="rhs">Stats to be added</param>
        /// <returns></returns>
            public static PrimaryAttributes operator +(PrimaryAttributes lhs, PrimaryAttributes rhs)
            {
            return new PrimaryAttributes
            (
                lhs.Vitality + rhs.Vitality,lhs.Strength + rhs.Strength,lhs.Dexterity + rhs.Dexterity, lhs.Intelligence + rhs.Intelligence);
            }
        }
}
