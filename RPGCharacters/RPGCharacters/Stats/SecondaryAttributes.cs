﻿namespace RPGCharacters
{
    public class SecondaryAttributes
    {
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementalResistance { get; set; }


        public SecondaryAttributes()
        {
            this.Health = 0;
            this.ArmorRating = 0;
            this.ElementalResistance = 0;
        }

        /// <summary>
        /// Calculates secondary stats given all primarystats
        /// </summary>
        /// <param name="vitality">gives life</param>
        /// <param name="strength">gives armor</param>
        /// <param name="dexterity">gives dexterity</param>
        /// <param name="intelligence">gives intelligence</param>
        /// <returns></returns>
        public SecondaryAttributes CalculateSecondaryAttributes(int vitality, int strength, int dexterity, int intelligence)
        {
            this.Health = vitality * 10;
            this.ArmorRating = strength + dexterity;
            this.ElementalResistance = intelligence;
            return this;
        }

        /// <summary>
        /// calculates secondary stats given the rpgcharacter
        /// </summary>
        /// <param name="character">character to update secondary stats</param>
        public void updateSecondaryAttributes(RPGCharacter character)
        {
            this.Health = character.TotalPrimaryAttribute.Vitality * 10;
            this.ArmorRating = character.TotalPrimaryAttribute.Strength + character.TotalPrimaryAttribute.Dexterity;
            this.ElementalResistance = character.TotalPrimaryAttribute.Intelligence;
        }
    }
}
