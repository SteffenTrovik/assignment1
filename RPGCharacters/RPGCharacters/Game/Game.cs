﻿using RPGCharacters.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Game
{
    class Game
    {
        /// <summary>
        /// Game class used to test out various aspects of the project, or a start to an amazing game.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            Console.Write("What is your name? ");
            string charName = Console.ReadLine();
            bool classChoice = true;
            RPGCharacter playerChar=null;
            while (classChoice)
            {
                Console.Write("\nWhat class would you like to be?(Mage/Ranger/Rogue/Warrior) ");
                string charClass = Console.ReadLine();
                switch (charClass)
                {
                    case "Mage":
                        playerChar = new Mage(charName);
                        classChoice = false;
                        break;
                    case "Ranger":
                        playerChar = new Ranger(charName);
                        classChoice = false;
                        break;
                    case "Rogue":
                        playerChar = new Rogue(charName);
                        classChoice = false;
                        break;
                    case "Warrior":
                        playerChar = new Warrior(charName);
                        classChoice = false;
                        break;
                    default:
                        break;
                }
            }
            Weapon testAxe = new Weapon("Common axe",1, Item.Slots.Weapon,Item.ItemTypes.Axe, Weapon.WeaponTypes.Axe,7,1.1);
            Weapon testBow = new Weapon("Common Bow", 1, Item.Slots.Weapon,Item.ItemTypes.Bow, Weapon.WeaponTypes.Axe, 12, 0.8);

            Armour testPlateBody = new Armour(new PrimaryAttributes(2, 1, 0, 0), "Common plate body armour", 1, Item.Slots.Body,Item.ItemTypes.Plate, Armour.ArmourTypes.Plate);
            Armour testClothHead = new Armour(new PrimaryAttributes(1,0,0,5), "Common cloth head armour", 1, Item.Slots.Head, Item.ItemTypes.Cloth,Armour.ArmourTypes.Cloth);


            while (true)
            {
                Console.WriteLine(playerChar);
                Console.WriteLine("Would you like to lvl up?(y/n) ");
                string ans = Console.ReadLine();
                if (ans == "y")
                {
                    playerChar.LevelUp();
                }
                if (playerChar.Level == 2)
                    Console.WriteLine("You get a " + testAxe);
                    EquipmentManager.EqupItem(playerChar, testAxe);
            }
        }
    }
}
