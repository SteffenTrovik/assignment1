﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static RPGCharacters.RPGCharacter;

namespace RPGCharacters
{
    public class Armour : Item
    {

        public PrimaryAttributes Attributes { get; set; }
        public enum ArmourTypes {Cloth, Leather, Mail, Plate};
        public ArmourTypes Armourtype { get; set; }

        /// <summary>
        /// Creates a new Armour item
        /// </summary>
        /// <param name="attributes">Stats on item</param>
        /// <param name="ItemName">Name of item</param>
        /// <param name="ItemLevel">Level of Item</param>
        /// <param name="ItemSlot">What slot on character item is</param>
        /// <param name="ItemType">What type of item it is</param>
        /// <param name="ArmourType">what specific armour type item is</param>
        public Armour(PrimaryAttributes attributes,string ItemName,int ItemLevel, Slots ItemSlot,ItemTypes ItemType, ArmourTypes ArmourType) : base(ItemName,ItemLevel, ItemSlot, ItemType)
        {
            this.Attributes= attributes;
            this.Armourtype = ArmourType;
        }
    }
}
