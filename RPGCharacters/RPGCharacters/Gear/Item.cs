﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public abstract class Item
    {

        public enum Slots { Head, Body, Legs, Weapon }
        public enum ItemTypes { Cloth, Leather, Mail, Plate, Staff, Wand, Bow, Dagger, Sword, Axe, Hammer }

        public string ItemName { get; set; }
        public int ItemLevel { get; set; }
        public Slots ItemSlot { get; set; }
        public ItemTypes ItemType { get; set; }

        public Item(string ItemName,int ItemLevel, Slots ItemSlot, ItemTypes ItemType)
        {
            this.ItemName = ItemName;
            this.ItemLevel = ItemLevel;
            this.ItemSlot = ItemSlot;
            this.ItemType=ItemType;
        }

        public override string ToString()
        {
            return "ItemName:\t"+ItemName;
        }
    }
}
