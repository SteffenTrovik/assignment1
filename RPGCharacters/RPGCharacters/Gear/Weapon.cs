﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters
{
    public class Weapon : Item
    {
        public enum WeaponTypes { Axe, Bow, Dagger, Hammer, Staff, Sword, Wand };
        public WeaponTypes WeaponType { get; set; }

        public int Damage { get; set; }
        public double AttackSpeed { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ItemName">Name of item</param>
        /// <param name="ItemLevel">Level of Item</param>
        /// <param name="ItemSlot">What slot on character item is</param>
        /// <param name="ItemType">What type of item item is</param>
        /// <param name="WeaponType">What type of weapon item is</param>
        /// <param name="Damage">The damage of one hit of theweapon</param>
        /// <param name="AttackSpeed">How often you can attack with the weapon</param>
        public Weapon(string ItemName,int ItemLevel, Slots ItemSlot,ItemTypes ItemType, WeaponTypes WeaponType, 
            int Damage, double AttackSpeed) : base(ItemName,ItemLevel, ItemSlot, ItemType)
        {
            this.WeaponType = WeaponType;
            this.Damage = Damage;
            this.AttackSpeed = AttackSpeed;
        }
    }
}
